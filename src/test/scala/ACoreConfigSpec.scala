package a_core_common

import org.scalatest._
import flatspec._
import matchers._

class ACoreConfigSpec extends AnyFlatSpec with should.Matchers {

  it should "initialize ACoreConfig from test.yaml" in {
    val config = ACoreConfig.loadFromFile("test.yaml")
    config.extension_set should be {Set(ISAExtension.M)}
    config.data_width should be {32}
    config.addr_width should be {32}
    config.progmem_depth should be {16}
    config.datamem_depth should be {16}
    config.bus_type should be {BusType.AXI4L}
    config.dCache.tag_memory_type should be {MemoryType.ASYNC}
    config.dCache.cache_memory_type should be {MemoryType.SYNC}
    config.dCache.depth should be {128}
    config.perf_monitors.hpm_counters should be {Set(3, 6)}
    config.dCache.line_depth should be {4}
    config.dCache.option should be {CacheOption.ALWAYS_ON}
    config.dCache.write_policy should be {WritePolicy.WRITE_THROUGH}
    config.debug should be {false}
    config.memory_bus.extra_masters.dmas.size should be {2}
    config.memory_bus.extra_masters.dmas(0).use should be {true}
    config.memory_bus.extra_masters.dmas(0).increment_depth should be {10}
    config.memory_bus.extra_masters.dmas(1).fifo_depth should be {3}
    config.memory_bus.extra_slaves.dma_controllers.size should be {2}
    config.memory_bus.extra_slaves.dma_controllers(0).address should be {"h3000_2000"}
    config.memory_bus.extra_slaves.dma_controllers(1).address should be {"h3000_3000"}
    println(config)
  }
}

// SPDX-License-Identifier: Apache-2.0

package a_core_common

import chisel3._
import chisel3.experimental._
import chisel3.util._
import pureconfig._
import pureconfig.generic.auto._
import pureconfig.generic.semiauto._
import pureconfig.generic.ProductHint
import pureconfig.module.yaml._
import BusType._
import MemoryType._
import CacheOption._
import WritePolicy._
import ISAExtension._

object ConfigHelpers {
  def isPowerOfTwo(x: Int): Boolean = { (x & (x - 1)) == 0 }
}

/** Cache configuration class.
  * 
  *
  * @param tag_memory_type Tag memory type
  * @param cache_memory_type Cache memory type
  * @param depth Number of cache lines
  * @param line_depth Number of words in a cache line
  * @param option Cache option (ALWAYS_ON, NONE, DYNAMIC)
  * @param write_policy Cache write policy
  *
  * @note  All fields are var's, so that they can be easily modified
  *        before providing the config as an input parameter for a class.
  *        However, the require-checks are done only during initialization, so
  *        it is recommended that the fields are modified only in tests.
  */
case class CacheConfig(
    var tag_memory_type: MemoryType = MemoryType.ASYNC,
    var cache_memory_type: MemoryType = MemoryType.SYNC,
    var depth: Int = 16,
    var line_depth: Int = 4,
    var option: CacheOption = CacheOption.NONE,
    var write_policy: WritePolicy = WritePolicy.WRITE_THROUGH
) {

  def validate() = {
    require(
      Seq(MemoryType.ASYNC).contains(tag_memory_type),
      "Only async data cache tag memory supported for now"
    )

    require(
      Seq(MemoryType.SYNC).contains(cache_memory_type),
      "Only synchronous data cache memory supported for now"
    )

    require(depth >= 2, "Cache must have at least two lines")
    require(ConfigHelpers.isPowerOfTwo(depth), "Cache depth must be a power of two")
    require(line_depth >= 2, "Cache line must have at least two words")

    require(
      ConfigHelpers.isPowerOfTwo(line_depth),
      "Cache line depth must be a power of two"
    )

    require(
      Seq(CacheOption.ALWAYS_ON, CacheOption.NONE).contains(option),
      "Cache option can be either ALWAYS_ON or NONE"
    )

    require(
      Seq(WritePolicy.WRITE_THROUGH).contains(write_policy),
      "Only write-through cache policy is supported"
    )
  }

  override def toString(): String = 
    s"""|CacheConfig (
        |    tag_memory_type   = $tag_memory_type,
        |    cache_memory_type = $cache_memory_type,
        |    depth             = $depth,
        |    line_depth        = $line_depth,
        |    option            = $option,
        |    write_policy      = $write_policy
        |  )""".stripMargin
}

/** Performance monitors configuration.
  * 
  *
  * @param hpm_counters List of enabled performance counters
  * @param use_load_indicator Use load indicator for PMU applications
  *
  * @note  Numbers 3-31 are allowed
  */
case class PerfMonitorConfig(
  var hpm_counters: Set[Int] = Set(),
  var use_load_indicator: Boolean = false
) {

  def validate() = {
    for (number <- hpm_counters) {
      require(3 <= number && number <= 31, "HPM counter should have a number between 3-31")
    }
    if (use_load_indicator) {
      require(hpm_counters.contains(3), "Load indicator can only be used with hpm counter 3")
    }
  }

  override def toString(): String = 
    s"""|PerfMonitorConfig (
        |    hpm_counters       = $hpm_counters,
        |    use_load_indicator = $use_load_indicator,
        |  )""".stripMargin
}

/**
  * Extra masters configuration
  *
  * @param dmas DMA configuration
  */
case class ExtraMastersConfig(
  var dmas: Seq[DMAConfig] = Seq(DMAConfig())
) {

  def validate() = {
    dmas.foreach(dma => dma.validate())
  }

  override def toString(): String = {
    ("|ExtraMastersConfig (\n" +
        {
          var body = ""
          for ((dma, i) <- dmas.zipWithIndex) {
            if (dma.use) {
              body += s"|      dma_$i = " + dma.toString() + ",\n"
            }
          }
          body += "|    )"
          body
        }
    ).stripMargin
  }
}

/**
  * Extra slaves configuration
  *
  * @param dma_controllers DMA controller configuration
  */
case class ExtraSlavesConfig(
  var dma_controllers: Seq[DMAControllerConfig] = Seq(DMAControllerConfig())
) {
  def validate() = {
    dma_controllers.foreach(dma_controller => dma_controller.validate())
  }
  override def toString(): String = {
    ("|ExtraSlavesConfig (\n" +
        {
          var body = ""
          for ((dma_controller, i) <- dma_controllers.zipWithIndex) {
            if (dma_controller.use) {
              body += s"|      dma_controller_$i = " + dma_controller.toString() + ",\n"
            }
          }
          body += "|    )"
          body
        }
    ).stripMargin
  }
}

/**
  * DMA Configuration
  *
  * @param use Use DMA
  * @param fifo_depth Internal FIFO depth (>=1)
  * @param increment_depth How many bits the increment includes. Maximum increment is 2^increment_depth.
  */
case class DMAConfig(
  var use: Boolean = false,
  var fifo_depth: Int = 1,
  var increment_depth: Int = 4
) {

  def validate() = {
    if (use) {
      require(fifo_depth >= 1, "DMA Fifo depth should be at least 1")
      require(increment_depth >= 1, "DMA increment depth should be at least 1")
    }
  }

  override def toString(): String = 
    s"""|DMAConfig (
        |        use = $use,
        |        fifo_depth = $fifo_depth,
        |        increment_depth = $increment_depth,
        |      )""".stripMargin
}

/**
  * DMA Controller configuration
  *
  * @param use Use DMA controller
  * @param address Slave address for DMA controller
  */
case class DMAControllerConfig(
  var use: Boolean = false,
  var address: String = "h3000_2000"
) {

  def validate() = {
    if (use) {
      require(address.take(1) == "h", "DMA controller slave address should be a Chisel hex string (e.g. h3000_2000)")
    }
  }

  override def toString(): String = 
    s"""|DMAControllerConfig (
        |        use = $use,
        |        address = $address,
        |      )""".stripMargin
}

/**
  * Memory bus configuration
  *
  * @param extra_masters Define extra masters
  * @param extra_slaves Define extra slaves
  */
case class MemoryBusConfig(
  var extra_masters: ExtraMastersConfig = ExtraMastersConfig(),
  var extra_slaves: ExtraSlavesConfig = ExtraSlavesConfig()
) {

  def validate() = {
    require(extra_masters.dmas.size == extra_slaves.dma_controllers.size,
            "You need to define the same number of DMAs and DMA Controllers")
    for ((dma, dma_controller) <- extra_masters.dmas.zip(extra_slaves.dma_controllers)) {
      if (dma.use || dma_controller.use) {
        require(dma.use && dma_controller.use,
                "You need to define DMA Controller if DMA has been defined, and vice versa")
      }
    }
    extra_masters.validate()
    extra_slaves.validate()
  }

  override def toString(): String = 
    s"""|MemoryBusConfig (
        |    extra_masters = $extra_masters,
        |    extra_slaves = $extra_slaves,
        |  )""".stripMargin
}

/** ACoreConfig configuration class.
 * 
 * @param data_width Data width
 * @param addr_width Address width
 * @param progmem_depth Program memory size, pow(2, progmem_depth)
 * @param datamem_depth Data memory size, pow(2, datamem_depth)
 * @param bus_type Data bus type
 * @param debug Enable debug interface
 * @param pc_init Program counter initialization value as Chisel hex string (e.g. h0000_1000)
 * @param dCache Cache configuration for data cache
 * @param perf_monitors Performance monitor configuration
 * @param memory_bus Memory bus configuration
 * @param extension_set Set of extensions to be enabled
 * 
 * @note  All fields are var's, so that they can be easily modified
 *        before providing the config as an input parameter for a class.
 *        However, the require-checks are done only during initialization, so
 *        it is recommended that the fields are modified only in tests.
*/
case class ACoreConfig(
    var data_width: Int,
    var addr_width: Int,
    var progmem_depth: Int,
    var datamem_depth: Int,
    var bus_type: BusType,
    var debug: Boolean,
    var pc_init: String,
    var dCache: CacheConfig = CacheConfig(),
    var perf_monitors: PerfMonitorConfig = PerfMonitorConfig(),
    var memory_bus: MemoryBusConfig = MemoryBusConfig(),
    var extension_set: Set[ISAExtension] = Set()
) {

  validate()

  def validate(): Unit = {
    for (ext <- extension_set) {
      ext match {
        case ISAExtension.Zicsr => {}
        case ISAExtension.M     => {}
        case ISAExtension.A     => {}
        case ISAExtension.F     => {}
        case ISAExtension.C     => {}
        case _ =>
          require(0 == 1, "Only M, A, F, C, and Zicsr extensions are supported")
      }
    }

    require(addr_width == 32, "Only 32-bit address width is supported")
    require(data_width == 32, "Only 32-bit data width is supported")
    require(progmem_depth > 0, "Program memory depth must be larger than 0")
    require(datamem_depth > 0, "Data memory depth must be larger than 0")

    require(
      Seq(BusType.AXI4L).contains(bus_type),
      "Only AXI4L bus type is supported"
    )

    require(pc_init.take(1) == "h", "pc_init should be a Chisel hex string (e.g. h0000_1000)")

    dCache.validate()
    perf_monitors.validate()
    memory_bus.validate()
  }

  def default_misa: Int = parseMisa(extension_set, data_width)

  /** Get cache size in bits */
  def getCacheSize(): Int = {
    dCache.depth * dCache.line_depth * data_width
  }

  /** How many masters are in the memory bus */
  def n_masters: Int = {
    var n = 1 // A-Core is one master by default
    // Add DMAs
    for (dma <- memory_bus.extra_masters.dmas) {
      if (dma.use) { n += 1 }
    }
    n
  }

  /** Construct default value for misa register from list of extensions */
  def parseMisa(extension_set: Set[ISAExtension], data_width: Int): Int = {
    var misa = 0
    misa |= (1 << 8)
    misa |= {
      if (data_width == 32) { 1 << 30 }
      else if (data_width == 64) { 1 << 63 }
      else if (data_width == 128) { (1 << 126) | (1 << 127) }
      else { 0 }
    }
    for (e <- extension_set) {
      e match {
        case ISAExtension.A => misa |= (1 << 0)
        case ISAExtension.C => misa |= (1 << 2)
        case ISAExtension.D => misa |= (1 << 3)
        case ISAExtension.F => misa |= (1 << 5)
        case ISAExtension.M => misa |= (1 << 12)
        case ISAExtension.V => misa |= (1 << 21)
        case _              => misa = misa
      }
    }
    misa
  }

  override def toString(): String = 
    s"""|ACoreConfig (
        |  extension_set = $extension_set,
        |  data_width    = $data_width,
        |  addr_width    = $addr_width,
        |  progmem_depth = $progmem_depth,
        |  datamem_depth = $datamem_depth,
        |  bus_type      = $bus_type,
        |  pc_init       = $pc_init,
        |  dCache        = $dCache,
        |  memory_bus    = $memory_bus,
        |  perf_monitors = $perf_monitors,
        |  debug         = $debug
        |)""".stripMargin

}

object ACoreConfig {

  /** Default configuration for A-Core */
  def default: ACoreConfig = {
    ACoreConfig(
      extension_set = Set(ISAExtension.M, ISAExtension.C, ISAExtension.F),
      addr_width = 32,
      data_width = 32,
      progmem_depth = 16,
      datamem_depth = 16,
      bus_type = BusType.AXI4L,
      pc_init = "h1000",
      dCache = CacheConfig(
        tag_memory_type = MemoryType.ASYNC,
        cache_memory_type = MemoryType.SYNC,
        depth = 128,
        line_depth = 4,
        option = CacheOption.ALWAYS_ON,
        write_policy = WritePolicy.WRITE_THROUGH
      ),
      debug = false
    )

  }

  /**
    * Create ACoreConfig from a config file.
    *
    * @param filename .conf file
    * @return ACoreConfig
    */
  def loadFromFile(filename: String): ACoreConfig = {

    /**
      * Dummy class for mapping isa_string into extension_set.
      * I couldn't find an easier way so the flow goes like this:
      * YAML -> ACoreConfigString (only isa_string)
      * YAML -> ACoreConfig (everything except isa_string)
      * Returns a copy of ACoreConfig, where extension_set = ACoreConfigString.isa_string
      *
      * @param isa_string
      */
    case class ACoreConfigString(isa_string : Set[ISAExtension])

    // Enums (implemented with sealed traits) require their convert functions
    // defined like this
    // ConfigFieldMapping ensures that there is no name format change from YAML to scala
    implicit val memoryTypeConvert: ConfigReader[MemoryType] =
      deriveEnumerationReader[MemoryType](
        ConfigFieldMapping(SnakeCase, SnakeCase)
      )
    implicit val cacheOptionConvert: ConfigReader[CacheOption] =
      deriveEnumerationReader[CacheOption](
        ConfigFieldMapping(SnakeCase, SnakeCase)
      )
    implicit val writePolicyConvert: ConfigReader[WritePolicy] =
      deriveEnumerationReader[WritePolicy](
        ConfigFieldMapping(SnakeCase, SnakeCase)
      )
    implicit val busTypeConvert: ConfigReader[BusType] =
      deriveEnumerationReader[BusType](ConfigFieldMapping(SnakeCase, SnakeCase))

    implicit val extensionSetReader: ConfigReader[Set[ISAExtension]] = 
      ConfigReader[String].map(parseExtensionSet(_))

    // Again, ensures that there is no name format change from YAML to scala
    // Sadly, this needs to be defined for each config class separately
    implicit val accProdHint: ProductHint[ACoreConfig] = ProductHint[ACoreConfig](ConfigFieldMapping(SnakeCase, SnakeCase))
    implicit val accStrProdHint: ProductHint[ACoreConfigString] = ProductHint[ACoreConfigString](ConfigFieldMapping(SnakeCase, SnakeCase))
    implicit val cacheProdHint: ProductHint[CacheConfig] = ProductHint[CacheConfig](ConfigFieldMapping(SnakeCase, SnakeCase))
    implicit val perfProdHint: ProductHint[PerfMonitorConfig] = ProductHint[PerfMonitorConfig](ConfigFieldMapping(SnakeCase, SnakeCase))
    implicit val memBusProdHint: ProductHint[MemoryBusConfig] = ProductHint[MemoryBusConfig](ConfigFieldMapping(SnakeCase, SnakeCase))
    implicit val exMasProdHint: ProductHint[ExtraMastersConfig] = ProductHint[ExtraMastersConfig](ConfigFieldMapping(SnakeCase, SnakeCase))
    implicit val exSlavProdHint: ProductHint[ExtraSlavesConfig] = ProductHint[ExtraSlavesConfig](ConfigFieldMapping(SnakeCase, SnakeCase))
    implicit val dmaConfProdHint: ProductHint[DMAConfig] = ProductHint[DMAConfig](ConfigFieldMapping(SnakeCase, SnakeCase))
    implicit val dmaContrProdHint: ProductHint[DMAControllerConfig] = ProductHint[DMAControllerConfig](ConfigFieldMapping(SnakeCase, SnakeCase))



    val source = YamlConfigSource.file(filename)
    val config = source.loadOrThrow[ACoreConfig]
    val configString = source.loadOrThrow[ACoreConfigString]
    config.copy(extension_set = configString.isa_string)
  }

  def main(args: Array[String]): Unit = {
    val config = loadFromFile("test.yaml")
  }

  /**
    * Parse extension set from isa string.
    *
    * @param isa_string isa string, e.g. 'rv32im'
    * @return
    */
  def parseExtensionSet(isa_string: String): Set[ISAExtension] = {
    // Default configuration values
    var XLEN: Int  = 32
    var zlen: Int  = 0
    var nregs: Int = 32

    var extensions: Set[ISAExtension] = Set()
    // match base ISA string
    val base_re = s"^rv(32|64|128)(i|e|g)".r
    val base_match =
      base_re.findFirstMatchIn(isa_string.toLowerCase).getOrElse {
        throw new IllegalArgumentException(
          """Error: "-isa_string" option must begin with: "rv32" | "rv64" | "rv128"
                |  followed by \"i\" | \"e\" | \"g\". (e.g. "-isa_string rv32i\")""".stripMargin
        )
      }

    // extract values from base match
    val new_xlen = base_match.group(1).toInt
    val new_nregs = base_match.group(2) match {
      case "i" => 32
      case "g" => 32
      case "e" => 16
    }

    // only allow E base with XLEN=32
    if (new_nregs == 16 && new_xlen != 32) {
      throw new IllegalArgumentException(
        "Error: The \"e\" base ISA can only be used with XLEN=32 (e.g. \"-isa_string rv32e\")"
      )
    }

    // parse G extension here since it is in the same place as I or E
    var exts = Set[ISAExtension]()
    if (base_match.group(2) == "g") {
      exts |= Set(
        ISAExtension.M,
        ISAExtension.A,
        ISAExtension.F,
        ISAExtension.D,
        ISAExtension.Zicsr,
        ISAExtension.Zifencei
      )
    }

    // drop the "rv32x" part and match base ISA character and extensions
    val exts_string = isa_string.drop(base_match.group(0).length)
    val exts_re     = "m|a|f|d|c|v|zicsr|zifencei".r
    for (ext_match <- exts_re.findAllMatchIn(exts_string.toLowerCase)) {
      ext_match.group(0) match {
        case "m" => exts += ISAExtension.M
        case "a" => exts += ISAExtension.A
        case "f" =>
          exts += ISAExtension.Zicsr
          exts += ISAExtension.F
        case "d"        => exts += ISAExtension.D
        case "c"        => exts += ISAExtension.C
        case "v"        => exts += ISAExtension.V
        case "zicsr"    => exts += ISAExtension.Zicsr
        case "zifencei" => exts += ISAExtension.Zifencei
        case e => {
          throw new IllegalArgumentException(
            "Error: Encountered unknown ISA extension: \"" + e + "\""
          )
        }
      }
    }

    if (exts.contains(ISAExtension.F)) { zlen = 32 }
    else if (exts.contains(ISAExtension.D)) { zlen = 64 }
    // else if (exts.contains(ISAExtension.H)) { zlen = 16 }
    // else if (exts.contains(ISAExtension.Q)) { zlen = 128 }

    XLEN = new_xlen
    nregs = new_nregs
    extensions = exts
    extensions
  }


}

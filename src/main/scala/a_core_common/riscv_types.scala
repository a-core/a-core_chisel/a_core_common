// SPDX-License-Identifier: Apache-2.0

package a_core_common

import chisel3._
import chisel3.experimental._
import chisel3.util._

// In general variables and constands are first defined in Scala and then mapped to Chisel

/** Scala definitions for RV32I opcodes represented as strings */
trait RV32I_Opcodes {
  val LOAD      = "b0000011"
  val LOAD_FP   = "b0000111"
  val custom_0  = "b0001011"
  val MISC_MEM  = "b0001111"
  val OP_IMM    = "b0010011"
  val AUIPC     = "b0010111"
  val OP_IMM_32 = "b0011011"

  val STORE     = "b0100011"
  val STORE_FP  = "b0100111"
  val custom_1  = "b0101011"
  val AMO       = "b0101111"
  val OP        = "b0110011"
  val LUI       = "b0110111"
  val OP_32     = "b0111011"

  val MADD      = "b1000011"
  val MSUB      = "b1000111"
  val NMSUB     = "b1001011"
  val NMADD     = "b1001111"
  val OP_FP     = "b1010011"
  val reserved_1= "b1010111"
  val custom_2  = "b1011011"
   
  val BRANCH    = "b1100011"
  val JALR      = "b1100111"
  val reserved_2= "b1101011"
  val JAL       = "b1101111"
  val SYSTEM    = "b1110011"
  val reserved_3= "b1110111"
  val custom_3  = "b1111011"
}

/** Contains RV32I opcodes represented as binary strings.
  * Used for defining the Opcode chisel enum.
  */
private object OPCODES extends RV32I_Opcodes {}

/** Chisel enumeration of RV32I opcodes */
object Opcode extends ChiselEnum
{
  val LOAD        = Value(OPCODES.LOAD.U)
  val LOAD_FP     = Value(OPCODES.LOAD_FP.U)
  val custom_0    = Value(OPCODES.custom_0.U)
  val MISC_MEM    = Value(OPCODES.MISC_MEM.U)
  val OP_IMM      = Value(OPCODES.OP_IMM.U)
  val AUIPC       = Value(OPCODES.AUIPC.U)
  val OP_IMM_32   = Value(OPCODES.OP_IMM_32.U)
  val STORE       = Value(OPCODES.STORE.U)
  val STORE_FP    = Value(OPCODES.STORE_FP.U)
  val custom_1    = Value(OPCODES.custom_1.U)
  val AMO         = Value(OPCODES.AMO.U)
  val OP          = Value(OPCODES.OP.U)
  val LUI         = Value(OPCODES.LUI.U)
  val OP_32       = Value(OPCODES.OP_32.U)
  val MADD        = Value(OPCODES.MADD.U)
  val MSUB        = Value(OPCODES.MSUB.U)
  val NMSUB       = Value(OPCODES.NMSUB.U)
  val NMADD       = Value(OPCODES.NMADD.U)
  val OP_FP       = Value(OPCODES.OP_FP.U)
  val reserved_1  = Value(OPCODES.reserved_1.U)
  val custom_2    = Value(OPCODES.custom_2.U)
  val BRANCH      = Value(OPCODES.BRANCH.U)
  val JALR        = Value(OPCODES.JALR.U)
  val reserved_2  = Value(OPCODES.reserved_2.U)
  val JAL         = Value(OPCODES.JAL.U)
  val SYSTEM      = Value(OPCODES.SYSTEM.U)
  val reserved_3  = Value(OPCODES.reserved_3.U)
  val custom_3    = Value(OPCODES.custom_3.U)
}

/** Chisel enumeration of RV32I instructions */
object Instruction extends ChiselEnum {
  val INVALID,  // Instruction could not be decoded
      LUI,      // Load upper immediate
      AUIPC,
      JAL,
      JALR,
      BEQ,
      BNE,
      BLT,
      BGE,
      BLTU,
      BGEU,
      LB,
      LH,
      LW,
      LBU,
      LHU,
      SB,
      SH,
      SW,
      ADDI,
      SLTI,
      SLTIU,
      XORI,
      ORI,
      ANDI,
      SLLI,
      SRLI,
      SRAI,
      ADD,
      SUB,
      SLL,
      SLT,
      SLTU,
      XOR,
      SRL,
      SRA,
      OR,
      AND,
      FENCE,
      ECALL,
      EBREAK,
      MRET,
/** Chisel enumeration of RV32M standard extension instructions */
      MUL,
      MULH,
      MULHSU,
      MULHU,
      DIV,
      DIVU,
      REM,
      REMU,
/** Chisel enumeration of RV32 Zicsr standard extension instructions */
      CSRRW,
      CSRRS,
      CSRRC,
      CSRRWI,
      CSRRSI,
      CSRRCI,
/** Chisel enumeration of RV32F standard extension instructions */
      FLW,	   // Load single float value from memory
      FSW,         // Store single float value to memory
      FRCSR,	   // Read fcsr
      FSCSR,	   // Write fcsr
      FRRM,	   // Read rounding mode
      FSRM,	   // Write rounding mode
      FRFLAGS,	   // Read fault flags
      FSFLAGS,	   // Write fault flags
      FADD_S,
      FSUB_S,
      FMUL_S,
      FDIV_S,
      FSQRT_S,
      FMIN_S,
      FMAX_S,
      FMADD_S,
      FMSUB_S, 
      FNMADD_S,
      FNMSUB_S,  
      FCVT_W_S,
      FCVT_S_W,
      FCVT_WU_S,
      FCVT_S_WU,
      FSGNJ_S,
      FSGNJN_S,
      FSGNJX_S,
      FMV_X_W,
      FMV_W_X,
      FCMP_FEQ_S,
      FCMP_FLT_S,
      FCMP_FLE_S,
      FCLASS_S,
/** Chisel enumeration of RV32A standard extension instructions */
      LR_W,
      SC_W,
      AMOSWAP_W,
      AMOADD_W,
      AMOXOR_W,
      AMOAND_W,
      AMOOR_W,
      AMOMIN_W,
      AMOMAX_W,
      AMOMINU_W,
      AMOMAXU_W
      = Value
}

/** Chisel enumeration of RV32I instruction types */
object InstrType extends ChiselEnum {
  val INVALID, R, I, S, B, U, J, CR, CI, CI16, CIP, CSS, CIW, CL, CS, CA, CB, CJ = Value
}

/** Enumeration of alu unit operations */
object ALUOp extends ChiselEnum {
  val ADD,  // addition
      SUB,  // subtraction 
      AND,  // bitwise and
      OR,   // bitwise or
      XOR,  // bitwise exclusive or
      LT,   // less than (A < B), numbers treated as signed
      LTU,  // less than (A < B), numbers treated as unsigned
      SLL,  // logical left shift (a << b)
      SRL,  // logical right shift (a >> b)
      SRA,  // arithmetic right shift (a >> b)
      ADD_JALR, // add operands and set least significant bit of the result to zero
      FWD_A // forward input a to output unmodified
      = Value
}

/** Enumeration of mult unit operations */
object MultOp extends ChiselEnum {
  val MUL,      // multiplies two operations and stores the lower 32 bits
      MULH,     // multiplies two signed operands and stores the upper 32 bits
      MULHU,    // multiplies two unsigned operands and stores the upper 32 bits
      MULHSU,   // multiplies signed and unsigned operands and stores the upper 32 bits
                // for divison: dividens = divisor x quotient + reminder
      DIV,      // divides two signed operands and stores answer rounded towards zero 
      DIVU,     // divides two unsigned operands and stores answer rounded towards zero
      REM,      // divides two signed operands and stores reminder
      REMU,      // divides two unsigned operands and stores reminder
      NULL
      = Value 
}

/** Enumeration of Zicsr unit operations */
object CSROp extends ChiselEnum {
  val CSRRW,    //
      CSRRS,    //
      CSRRC,    //
      CSRRWI,   //
      CSRRSI,   //
      CSRRCI,    //
      NULL
      = Value 
}

/** Enumeration of floating point unit operations */
object FPUOp extends ChiselEnum {
  val FADD_S,      // Single precision Floating-point addition	( rs1 + rs2 )
      FSUB_S,      // SPFP subtraction				( rs1 - rs2 )
      FMUL_S,      // SPFP multiplication			( rs1 * rs2 )
      FDIV_S,      // SPFP division				( rs1 / rs2 )
      FSQRT_S,     // SPFP square root				( SQRT(rs1) )
      FMIN_S,      // SPFP minimum				( MIN(rs1, rs2) )
      FMAX_S,      // SPFP maximum				( MAX(rs1, rs2) )
      FMADD_S,     // SPFP fused multiply addition		( rs1 * rs2 ) + rs3
      FMSUB_S,     // SPFP fused multiply subtraction		( rs1 * rs2 ) - rs3
      FNMADD_S,    // SPFP fused negative multiply addition	-( rs1 * rs2 ) + rs3
      FNMSUB_S,    // SPFP fused negative multiply subtraction	-( rs1 * rs2 ) - rs3
      FCMP_FEQ_S,  // SPFP equals 				( rs1 == rs2 )
      FCMP_FLT_S,  // SPFP less than 				( rs1 < rs2 )
      FCMP_FLE_S,  // SPFP less than OR equal 			( rs1 <= rs2 )
      FCVT_W_S,    // SPFP      ->   32b int
      FCVT_S_W,    // 32b int   ->   SPFP
      FCVT_WU_S,   // SPFP      ->   32b Uint
      FCVT_S_WU,   // 32b Uint  ->   SPFP
      FSGNJ_S,     // sign bit is rs2's sign bit
      FSGNJN_S,    // sign bit is opposite of r2's sign bit
      FSGNJX_S,    // sign bit is XOR of rs1 & rs2     
      FMV_X_W,     // SPFP reg value to Integer reg
      FMV_W_X,     // Integer reg value to SPFP reg 
      FCLASS_S,     // Identify type of the SPFP
      NULL
      = Value 
}

/** Enumeration of floating point rounding types (for int or float) */
object RoundingType extends ChiselEnum {
  val FLOAT = Value(0.U)
  val UINT  = Value(1.U)
  val SINT  = Value(2.U)
  val SPEC  = Value(3.U)
}

/** Enumeration of floating point rounding modes */
object RoundingMode extends ChiselEnum {
  val RNE,
      RTZ,
      RDN,
      RUP,
      RMM,
      reserved_1,
      reserved_2,
      DYN,
      NULL
      = Value 
}

/** Enumeration of floating point fault flags */
object FaultFlag extends ChiselEnum {
  val NX = Value(0.U)
  val UF = Value(1.U)
  val OF = Value(2.U)
  val DZ = Value(3.U)
  val NV = Value(4.U)
  val NULL = Value(5.U)  
}

/** Enumeration of floating point widths */
object FloatingPointFormat extends ChiselEnum {
  val S = Value(32.U)
  val D = Value(64.U)
  val H = Value(16.U)
  val Q = Value(128.U)
}

/**
  * Exception causes
  * The value corresponds to the encoding in mcause register
  *
  */
object ExceptionCauses extends ChiselEnum {
  val INSTR_ADDR_MISALIG = Value(0.U)
  val INSTR_ACCESS_FAULT = Value(1.U)
  val ILLEGAL_INSTR      = Value(2.U)
  val BREAKPOINT         = Value(3.U)
  val LOAD_ADDR_MISALIG  = Value(4.U)
  val LOAD_ACCESS_FAULT  = Value(5.U)
  val STORE_ADDR_MISALIG = Value(6.U)
  val STORE_ACCESS_FAULT = Value(7.U)
  val ENV_CALL_FROM_M    = Value(11.U)
}

sealed trait ISAExtension
/** Enumeration of RISC-V ISA extensions */
object ISAExtension {
  case object M extends ISAExtension
  case object A extends ISAExtension
  case object F extends ISAExtension
  case object D extends ISAExtension
  case object G extends ISAExtension
  case object C extends ISAExtension
  case object V extends ISAExtension
  case object Zicsr extends ISAExtension
  case object Zifencei extends ISAExtension
}

/** Type for interpreting data as a R type instruction */
class InstrTypeR extends Bundle {
  val funct7    = UInt(7.W)
  val rs2       = UInt(5.W)
  val rs1       = UInt(5.W)
  val funct3    = UInt(3.W)
  val rd        = UInt(5.W)
  val opcode    = UInt(7.W)
}

/** Type for interpreting data as an I type instruction */
class InstrTypeI extends Bundle {
  val imm_11_0  = Bits(12.W)
  val rs1       = UInt(5.W)
  val funct3    = UInt(3.W)
  val rd        = UInt(5.W)
  val opcode    = UInt(7.W)
}

/** Type for interpreting data as a S type instruction */
class InstrTypeS extends Bundle {
  val imm_11_5  = Bits(7.W)
  val rs2       = UInt(5.W)
  val rs1       = UInt(5.W)
  val funct3    = UInt(3.W)
  val imm_4_0   = Bits(5.W)
  val opcode    = UInt(7.W)
}

/** Type for interpreting data as a B type instruction */
class InstrTypeB extends Bundle {
  val imm_12    = Bool()
  val imm_10_5  = Bits(6.W)
  val rs2       = UInt(5.W)
  val rs1       = UInt(5.W)
  val funct3    = UInt(3.W)
  val imm_4_1   = Bits(4.W)
  val imm_11    = Bool()
  val opcode    = UInt(7.W)
}

/** Type for interpreting data as a U type instruction */
class InstrTypeU extends Bundle {
  val imm_31_12 = Bits(20.W)
  val rd        = UInt(5.W)
  val opcode    = UInt(7.W)
}

/** Type for interpreting data as a J type instruction */
class InstrTypeJ extends Bundle {
  val imm_20    = Bool()
  val imm_10_1  = Bits(10.W)
  val imm_11    = Bool()
  val imm_19_12 = Bits(8.W)
  val rd        = UInt(5.W)
  val opcode    = UInt(7.W)
}

class InstrTypeCR extends Bundle {
  val padding = UInt(16.W)
  val funct4 = UInt(4.W)
  val rd = UInt(5.W)
  val rs2 = UInt(5.W)
  val opcode = UInt(2.W)
}

class InstrTypeCI extends Bundle {
  val padding = UInt(16.W)
  val funct3 = UInt(3.W)
  val imm_5 = Bool()
  val rd = UInt(5.W)
  val imm_4_0 = Bits(5.W)
  val opcode = UInt(2.W)
}

class InstrTypeCI16 extends Bundle {
  val padding = UInt(16.W)
  val funct3 = UInt(3.W)
  val imm_9 = Bool()
  val rd = UInt(5.W)
  val imm_4 = Bool()
  val imm_6 = Bool()
  val imm_8_7 = UInt(2.W)
  val imm_5 = Bool()
  val opcode = UInt(2.W)
}

class InstrTypeCIP extends Bundle {
  val padding = UInt(16.W)
  val funct3 = UInt(3.W)
  val imm_5 = Bool()
  val rd = UInt(5.W)
  val imm_4_2 = UInt(3.W)
  val imm_7_6 = UInt(2.W)
  val opcode = UInt(2.W)
}

class InstrTypeCSS extends Bundle {
  val padding = UInt(16.W)
  val funct3 = UInt(3.W)
  val imm_5_2 = Bits(4.W)
  val imm_7_6 = Bits(2.W)
  val rs2 = UInt(5.W)
  val opcode = UInt(2.W)
}

class InstrTypeCIW extends Bundle {
  val padding = UInt(16.W)
  val funct3 = UInt(3.W)
  val imm_5_4 = Bits(2.W)
  val imm_9_6 = Bits(4.W)
  val imm_2 = Bool()
  val imm_3 = Bool()
  val rd = UInt(3.W)
  val opcode = UInt(2.W)
}

class InstrTypeCL extends Bundle {
  val padding = UInt(16.W)
  val funct3 = UInt(3.W)
  val imm_3_1 = Bits(3.W)
  val rs1 = UInt(3.W)
  val imm_0 = Bool()
  val imm_4 = Bool()
  val rd = UInt(3.W)
  val opcode = UInt(2.W)
}

class InstrTypeCS extends Bundle {
  val padding = UInt(16.W)
  val funct3 = UInt(3.W)
  val imm_3_1 = Bits(3.W)
  val rd = UInt(3.W)
  val imm_0 = Bool()
  val imm_4 = Bool()
  val rs2 = UInt(3.W)
  val opcode = UInt(2.W)
}

class InstrTypeCB extends Bundle {
  val padding = UInt(16.W)
  val funct3 = UInt(3.W)
  val imm_8 = Bool()
  val imm_4_3 = UInt(2.W)
  val rs1 = UInt(3.W)
  val imm_7_6 = Bits(2.W)
  val imm_2_1 = Bits(2.W)
  val imm_5 = Bool()
  val opcode = UInt(2.W)
}

class InstrTypeCA extends Bundle {
  val padding = UInt(16.W)
  val funct6 = UInt(6.W)
  val rs1 = UInt(3.W)
  val funct2 = UInt(2.W)
  val rs2 = UInt(3.W)
  val opcode = UInt(2.W)
}

class InstrTypeCJ extends Bundle {
  val padding = UInt(16.W)
  val funct3 = UInt(3.W)
  val imm_11 = Bool()
  val imm_4 = Bool()
  val imm_9_8 = Bits(2.W)
  val imm_10 = Bool()
  val imm_6 = Bool()
  val imm_7 = Bool()
  val imm_3_1 = Bits(3.W)
  val imm_5 = Bool()
  val opcode = UInt(2.W)
}

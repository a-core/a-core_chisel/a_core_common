package a_core_common

import chisel3._
import chisel3.experimental._
import chisel3.util._

object riscv_util {
  def getFunct12(instr_bits: UInt) = {
    instr_bits(31, 20)
  }

  def getFunct3(instr_bits: UInt) = {
    instr_bits(14, 12)
  }

  def getFunct7(instr_bits: UInt) = {
    instr_bits(31, 25)
  }

  def getRs1(instr_bits: UInt) = {
    instr_bits(19, 15)
  }

  def getRs2(instr_bits: UInt) = {
    instr_bits(24, 20)
  }

  def getRs3(instr_bits: UInt) = {
    instr_bits(31, 27)
  }

  def getRd(instr_bits: UInt) = {
    instr_bits(11, 7)
  }

  def getOpcode(instr_bits: UInt) = {
    Opcode(instr_bits(6, 0))
  }
}
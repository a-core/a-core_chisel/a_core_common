// SPDX-License-Identifier: Apache-2.0

package a_core_common

import chisel3._
import chisel3.experimental._
import chisel3.util._

object MemOpWidth extends ChiselEnum {
  val BYTE = Value(0.U)
  val HWORD = Value(1.U)
  val WORD = Value(2.U)
  val DWORD = Value(3.U)  // for RV64I
}

sealed trait MemoryType
object MemoryType {
  case object ASYNC extends MemoryType
  case object SYNC extends MemoryType
}

sealed trait CacheOption
object CacheOption {
  case object NONE extends CacheOption
  case object ALWAYS_ON extends CacheOption
  case object DYNAMIC extends CacheOption
}

sealed trait WritePolicy
object WritePolicy {
  case object WRITE_BACK extends WritePolicy
  case object WRITE_THROUGH extends WritePolicy
}

/** Umbrella trait for all different kinds of LSU interfaces (cache -> memory) */
trait LSUPort {
  val lsu_port: Bundle
}

sealed trait BusType
/** Enumeration of available LSU master port buses */
object BusType {
  case object AXI4L extends BusType
  case object WishboneB4 extends BusType

  def fromString(s: String): BusType = s match {
    case "axi4l" => AXI4L
    case "wishbone_b4" => WishboneB4
    case s => throw new IllegalArgumentException(f"Invalid bus type: $s")
  }
}

/** Global A-Core configuration container */
object ACoreOptions {

  // Default configuration values
  var XLEN: Int = 32
  var zlen: Int = 0
  var nregs: Int = 32

  var extensions: Set[ISAExtension] = Set()
  var default_misa: Int = 0

  /** Set configuration options based on a RISC-V ISA string
    * @param isa_string RISC-V ISA string that determines the ISA base and available extensions.
    * @return a boolean that indicates if the operation succeeded.
    */
  def from(isa_string: String) : Boolean = {
    // match base ISA string
    val base_re = s"^rv(32|64|128)(i|e|g)".r
    val base_match = base_re.findFirstMatchIn(isa_string.toLowerCase).getOrElse {
      println("""Error: "-isa_string" option must begin with: "rv32" | "rv64" | "rv128"
                |  followed by \"i\" | \"e\" | \"g\". (e.g. "-isa_string rv32i\")""".stripMargin)
      return false
    }

    // extract values from base match
    val new_xlen = base_match.group(1).toInt
    val new_nregs = base_match.group(2) match {
      case "i" => 32
      case "g" => 32
      case "e" => 16
    }

    // only allow E base with XLEN=32
    if (new_nregs == 16 && new_xlen != 32) {
      println("Error: The \"e\" base ISA can only be used with XLEN=32 (e.g. \"-isa_string rv32e\")")
      return false
    }

    // parse G extension here since it is in the same place as I or E
    var exts = Set[ISAExtension]()
    if (base_match.group(2) == "g") {
      exts |= Set(
        ISAExtension.M,
        ISAExtension.A,
        ISAExtension.F,
        ISAExtension.D,
        ISAExtension.Zicsr,
        ISAExtension.Zifencei,
      )
    }

    // drop the "rv32x" part and match base ISA character and extensions
    val exts_string = isa_string.drop(base_match.group(0).length)
    val exts_re = "m|a|f|d|c|v|zicsr|zifencei".r
    for (ext_match <- exts_re.findAllMatchIn(exts_string.toLowerCase)) {
      ext_match.group(0) match {
        case "m" => exts += ISAExtension.M
        case "a" => exts += ISAExtension.A
        case "f" => exts += ISAExtension.Zicsr
                    exts += ISAExtension.F
        case "d" => exts += ISAExtension.D
        case "c" => exts += ISAExtension.C
        case "v" => exts += ISAExtension.V
        case "zicsr"    => exts += ISAExtension.Zicsr
        case "zifencei" => exts += ISAExtension.Zifencei
        case e => {
          println("Error: Encountered unknown ISA extension: \""+e+"\"")
          return false
        }
      }
    }
    
    // Construct default value for misa register from list of extensions
    var misa = 0
    misa |= (1 << 8) 
    misa |= {
              if      (new_xlen == 32)  {1 << 30}
              else if (new_xlen == 64)  {1 << 63}
              else if (new_xlen == 128) {(1 << 126) | (1 << 127)}
              else {0}
            }
    for (e <- exts) {
      e match {
        case ISAExtension.A => misa |= (1 << 0)
        case ISAExtension.C => misa |= (1 << 2)
        case ISAExtension.D => misa |= (1 << 3)
        case ISAExtension.F => misa |= (1 << 5)
        case ISAExtension.M => misa |= (1 << 12)
        case ISAExtension.V => misa |= (1 << 21)
        case _              => misa = misa
      }
    }


    if (exts.contains(ISAExtension.F)) { zlen = 32 } 
    else if (exts.contains(ISAExtension.D)) { zlen = 64 } 
    // else if (exts.contains(ISAExtension.H)) { zlen = 16 } 
    // else if (exts.contains(ISAExtension.Q)) { zlen = 128 } 



    XLEN = new_xlen
    nregs = new_nregs
    extensions = exts
    default_misa = misa
    true
  }

  override def toString : String = s"""
    |ACoreOptions {
    |  xlen=$XLEN,
    |  zlen=$zlen,
    |  nregs=$nregs,
    |  extensions=$extensions,
    |  default_misa=${"0x" + default_misa.toHexString}
    |}""".stripMargin
}

/* Instruction memory programming interface */
class ProgIface(depth_bits: Int) extends Bundle {
  val data  = Input(UInt(8.W))
  val addr  = Input(UInt(depth_bits.W))
}

/** Command-line option parser */
trait OptionParser {
  /** Recursively parse option flags from command line args
   * @param options Map of command line option names to their respective default values.
   * @param arguments List of arguments to parse.
   * @param help List of 3-tuples for generating help message with the following information (argument, type, message)
   * @return a tuple whose first element is the map of parsed options to their values 
   *         and the second element is the list of arguments that don't take any values.
   */
  def getopts(options: Map[String, String], arguments: List[String], help: Seq[(String, String, String)]) : (Map[String, String], List[String]) = {
    // Parse next elements in argument list
    arguments match {
      case "-h" :: tail => {
        val helpSeq = help :+ ("-h", "", "Show this help message.")
        println(s"Usage: ${this.getClass.getName.replace("$","")} [-<option> <argument>]")
        println("\nOptions")
        val paramMaxLength= helpSeq.map(_._1.length).max
        val typeMaxLength = helpSeq.map(_._2.length).max
        helpSeq.foreach(x => {
          val ppad = " "*(paramMaxLength - x._1.length)
          val tpad = " "*(typeMaxLength  - x._2.length)
          println(s"  ${x._1}$ppad [${x._2}]$tpad : ${x._3}")
        })
        sys.exit()
      }
      case option :: value :: tail if options contains option => {
        val (newopts, newargs) = getopts(options ++ Map(option -> value), tail, help)
        (newopts, newargs)
      }
      case argument :: tail => {
        val (newopts, newargs) = getopts(options, tail, help)
        (newopts, argument.toString +: newargs)
      }
      case Nil => (options, arguments)
    }
  }

  /** Print parsed options and arguments to stdout */
  def printopts(options: Map[String, String], arguments: List[String]) = {
      println("Command line options:")
      for ((k,v) <- options) {
        println(s"  $k = $v")
      }
      println("Command line arguments:")
      for (arg <- arguments) {
        println(s"  $arg")
      }
  }
}

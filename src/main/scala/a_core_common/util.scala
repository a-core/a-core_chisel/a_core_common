// SPDX-License-Identifier: Apache-2.0

package a_core_common

import chisel3._
import chisel3.experimental._
import chisel3.util._

// For common definitions
package object util {
  // Double-flop synchronizers or clock domain crossing
  // The suffix `_cdc_reg` can be used as a search term for timing constraints
  def synchronize(s: UInt) = {
    val first = RegNext(s)
    RegNext(first)
  }
}